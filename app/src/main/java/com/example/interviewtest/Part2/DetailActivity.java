package com.example.interviewtest.Part2;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.interviewtest.R;

public class DetailActivity extends AppCompatActivity {
    private TextView dname, demail, dbalance;
    private ImageView dimage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.part2);


        dname = findViewById(R.id.developername);
        demail = findViewById(R.id.developeremail);
        dbalance = findViewById(R.id.developerbalance);
        dimage = findViewById(R.id.developerimage);


        String detailname = getIntent().getExtras().getString("name");
        String detailemail = getIntent().getExtras().getString("email");
        String detailbalance = getIntent().getExtras().getString("balance");
        String detailimage = getIntent().getExtras().getString("picture");

        getSupportActionBar().setTitle(detailname);

        dname.setText(detailname);
        demail.setText(detailemail);
        dbalance.setText(detailbalance);
        Glide.with(this).load(detailimage).into(dimage);


    }
}
