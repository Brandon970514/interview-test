package com.example.interviewtest.Api;

import com.example.interviewtest.Part1.ListItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {

    String URL_DATA = "https://api.myjson.com/bins/";

    @GET("a9eh1")
    Call<List<ListItem>> getList();
}
