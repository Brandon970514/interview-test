package com.example.interviewtest.Part1;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.interviewtest.Part2.DetailActivity;
import com.example.interviewtest.R;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.DeveloperViewHolder> {

    private Context mContext;
    private List<ListItem> DeveloperList;

    public ListAdapter(Context context, List<ListItem> developerList){
        this.mContext = context;
        this.DeveloperList = developerList;
    }
    public void setList(List<ListItem> developerList) {
        this.DeveloperList = developerList;
        notifyDataSetChanged();
    }

    @Override
    public DeveloperViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_item,parent,false);
        return new DeveloperViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DeveloperViewHolder holder, int position) {
        ListItem currentItem = DeveloperList.get(position);

        String name = currentItem.getName();
        String phone = currentItem.getPhone();
        int age = currentItem.getAge();



            holder.txtname.setText(name);
            holder.txtphone.setText(phone);
            holder.txtage.setText(String.valueOf(age));

    }

    @Override
    public int getItemCount() {
        return DeveloperList.size();
    }

    public class DeveloperViewHolder extends RecyclerView.ViewHolder{
        public TextView txtname;
        public TextView txtphone;
        public TextView txtage;

        public DeveloperViewHolder(View itemView) {
            super(itemView);

            txtname = itemView.findViewById(R.id.developername1);
            txtphone = itemView.findViewById(R.id.developernumber1);
            txtage = itemView.findViewById(R.id.developerage);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        ListItem clickItem = DeveloperList.get(pos);
                        Intent intent = new Intent(mContext, DetailActivity.class);
                        intent.putExtra("balance", clickItem.getBalance());
                        intent.putExtra("picture", clickItem.getPicture());
                        intent.putExtra("age", clickItem.getAge());
                        intent.putExtra("email", clickItem.getEmail());
                        intent.putExtra("name", clickItem.getName());
                        intent.putExtra("gender", clickItem.getGender());
                        intent.putExtra("phone", clickItem.getPhone());
                        intent.putExtra("address", clickItem.getAddress());
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                        Toast.makeText(v.getContext(), "clicked " + clickItem.getName(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }


    }

}
