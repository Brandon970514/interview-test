package com.example.interviewtest.Part1;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

public class ListItem {

    @SerializedName("isActive")
    private String isActive;
    @SerializedName("balance")
    private String balance;
    @SerializedName("picture")
    private String picture;
    @SerializedName("age")
    private int age;
    @SerializedName("email")
    private String email;
    @SerializedName("name")
    private String name;
    @SerializedName("gender")
    private String gender;
    @SerializedName("phone")
    private String phone;
    @SerializedName("address")
    private String address;

    public ListItem(String isActive, String balance, String picture, int age, String email, String name, String gender, String phone, String address) {
        this.isActive = isActive;
        this.balance = balance;
        this.picture = picture;
        this.age = age;
        this.email = email;
        this.name = name;
        this.gender = gender;
        this.phone = phone;
        this.address = address;
    }

    public String getIsActive() {
        return isActive;
    }

    public String getBalance() {
        return balance;
    }

    public String getPicture() {
        return picture;
    }

    public int getAge() {
        return age;
    }

    public String getEmail(){ return email; }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public static final Comparator<ListItem> BY_DEVELOPER_AGE = new Comparator<ListItem>() {
        @Override
        public int compare(ListItem o1, ListItem o2) {
            return  (o1.getAge()>o2.getAge() ? -1 : (o1.getAge()==o2.getAge() ? 0 : 1));
        }
    };


}
