package com.example.interviewtest;

import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.interviewtest.Api.Api;
import com.example.interviewtest.Part1.ListAdapter;
import com.example.interviewtest.Part1.ListItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {

        private Toast backToast;
        boolean secondBack;


    private RecyclerView recyclerView;
    private ListAdapter listAdapter;
    private ProgressDialog progressDialog;

    private List<ListItem> DeveloperList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create toast in advance
        backToast = Toast.makeText(this, "Press Back again to quit", Toast.LENGTH_SHORT);
        secondBack = false;

        //init
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        recyclerView = findViewById(R.id.recycler_viewDeveloper);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        DeveloperList = new ArrayList<>();
        listAdapter = new ListAdapter(getApplicationContext(),DeveloperList);
        recyclerView.setAdapter(listAdapter);

        parseJSON();
    }

    private void parseJSON() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.URL_DATA)
//                .baseUrl("https://api.myjson.com/bins/a9eh1")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        Api api = retrofit.create(Api.class);
        Log.d("API",api.toString());

        Call<List<ListItem>> call = api.getList();

        call.enqueue(new Callback<List<ListItem>>() {
            @Override
            public void onResponse(Call<List<ListItem>> call, Response<List<ListItem>> response) {
                Log.d("Text",response.toString());


                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Server returned data", Toast.LENGTH_SHORT).show();
                    DeveloperList = response.body();

                    Collections.sort(DeveloperList, ListItem.BY_DEVELOPER_AGE);

                    ArrayList<ListItem> developers = new ArrayList();
                    for(ListItem item : DeveloperList) {
                        if(item.getIsActive().equalsIgnoreCase("true")) {
                            developers.add(item);
                        }
                    }

                    listAdapter.setList(developers);
                    progressDialog.hide();

                }
                else {
                    Toast.makeText(getApplicationContext(), "Server returned an error", Toast.LENGTH_SHORT).show();
                    progressDialog.hide();
                }

            }

            @Override
            public void onFailure(Call<List<ListItem>> call, Throwable t) {
                progressDialog.hide();
                Log.d("Text",t.toString());
                Toast.makeText(getApplicationContext(),t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    final Handler backHandler = new Handler();

    @Override
    public void onBackPressed() {
        if (secondBack) {
            // Cancel the toast to make it stop showing
            backToast.cancel();
            finish();
        } else {
            // Be ready for the second Back
            secondBack = true;
            // Show toast
            backToast.show();
            // Initiate timeout
            backHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    secondBack = false;
                }
            }, 2000); // Toast.LENGTH_SHORT lasts for 2000 ms
        }
    }

}
